class Project < ApplicationRecord
  belongs_to :user
  acts_as_tree

  require 'iota'
  after_create :create_address

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders, :history]

  # VALIDATIONS
  validates :title, length: { minimum: 1, maximum: 100 }

  private

  def create_address
    # Create client directly with provider
    client = IOTA::Client.new(provider: 'https://iota.nodes24.com')

    # now you can start using all of the functions
    #status, data = client.api.getNodeInfo

    ## create and save seed
    self.seed = (1..81).map{'ABCDEFGHIJKLMNOPQRSTUVWXYZ9'[SecureRandom.random_number(27)]}.join('')

    ## create and save latest address
    account = IOTA::Models::Account.new(client, self.seed)
    account.getAccountDetails({})

    self.address = account.latestAddress
    self.save!

  end

end
